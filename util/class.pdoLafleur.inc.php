﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application lafleur
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
 * @package default
 * @author Patrice Grand
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoLafleur
{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=gsb';
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoLafleur = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
    		PdoLafleur::$monPdo = new PDO(PdoLafleur::$serveur.';'.PdoLafleur::$bdd, PdoLafleur::$user, PdoLafleur::$mdp); 
			PdoLafleur::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoLafleur::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : $instancePdolafleur = PdoLafleur::getPdoLafleur();
 * @return l'unique objet de la classe PdoLafleur
 */
	public  static function getPdoLafleur()
	{
		if(PdoLafleur::$monPdoLafleur == null)
		{
			PdoLafleur::$monPdoLafleur= new PdoLafleur();
		}
		return PdoLafleur::$monPdoLafleur;  
	}
/**
 * Retourne toutes les catégories sous forme d'un tableau associatif
 *
 * @return le tableau associatif des catégories 
*/
	public function getLesCategories()
	{
		$req = "select * from categorie";
		$res = PdoLafleur::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

/**
 * Retourne sous forme d'un tableau associatif tous les produits de la
 * catégorie passée en argument
 * 
 * @param $idCategorie 
 * @return un tableau associatif  
*/

	public function getLesProduitsDeCategorie($idCategorie)
	{
	    $req="select * from produit where idCategorie = '$idCategorie'";
		$res = PdoLafleur::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes; 
	}
/**
 * Retourne les produits concernés par le tableau des idProduits passée en argument
 *
 * @param $desIdProduit tableau d'idProduits
 * @return un tableau associatif 
*/
	public function getLesProduitsDuTableau($desIdProduit)
	{
		$nbProduits = count($desIdProduit);
		$lesProduits=array();
		if($nbProduits != 0)
		{
			foreach($desIdProduit as $unIdProduit)
			{
				$req = "select * from produit where id = '$unIdProduit'";
				$res = PdoLafleur::$monPdo->query($req);
				$unProduit = $res->fetch();
				$lesProduits[] = $unProduit;
			}
		}
		return $lesProduits;
	}
/**
 * Crée une commande 
 *
 * Crée une commande à partir des arguments validés passés en paramètre, l'identifiant est
 * construit à partir du maximum existant ; crée les lignes de commandes dans la table contenir à partir du
 * tableau d'idProduit passé en paramètre
 * @param $nom 
 * @param $rue
 * @param $cp
 * @param $ville
 * @param $mail
 * @param $lesIdProduit
 
*/
	public function creerCommande($nom,$rue,$cp,$ville,$mail, $lesIdProduit, $lesqtes )
	{
		$req = "select max(id) as maxi from commande";
		echo $req."<br>";
		$res = PdoLafleur::$monPdo->query($req);
		$laLigne = $res->fetch();
		$maxi = $laLigne['maxi'] ;
		$maxi++;
		$idCommande = $maxi;
		echo $idCommande."<br>";
		echo $maxi."<br>";
		$date = date('Y/m/d');
		$req = "insert into commande values ('$idCommande','$date','$nom','$rue','$cp','$ville','$mail')";
		echo $req."<br>";
		$res = PdoLafleur::$monPdo->exec($req);
        $pos=0;
		foreach($lesIdProduit as $unIdProduit)
		{
			$req = "insert into contenir values ('$idCommande','$unIdProduit', '$lesqtes[$pos]')";
			echo $req."<br>";
			$res = PdoLafleur::$monPdo->exec($req);
            $pos++;
		}

		
	
	}

    public function verifLogin($login,$mdp)
    {
        $req = "SELECT count(*) as nb FROM administrateur where nom='$login' and mdp='$mdp'";
        $res = PdoLafleur::$monPdo->query($req);
        $laLigne = $res->fetch();
        $nb = $laLigne['nb'];
        if ($nb == 0) {
            return false;
        } else {
            return true;
        }

    }

    public function voirCdes()
    {
        $req = "select * from commande";
        $res = PdoLafleur::$monPdo->query($req);
        $lesCdes = $res->fetchAll();
        return $lesCdes;
    }

    public function voirCats()
    {
        $req = "select * from categorie";
        $res = PdoLafleur::$monPdo->query($req);
        $lesCats = $res->fetchAll();
        return $lesCats;
    }

    public function inscriptionClient($nom,$prenom,$adresse,$cp,$ville,$mail,$pwd)
    {
        $req = "INSERT INTO client (nomClient, prenomClient, mailClient, adresseClient, cpClient, villeClient, mdpClient) VALUES ('$nom','$prenom','$mail','$adresse','$cp','$ville','$pwd');";
        $res = PdoLafleur::$monPdo->query($req);

    }

    public function verifClient($mail,$mdp)
    {
        $req = "SELECT count(*) as nb FROM client where mailClient='$mail' and mdpClient='$mdp'";
        $res = PdoLafleur::$monPdo->query($req);
        $laLigne = $res->fetch();
        $nb = $laLigne['nb'];
        if ($nb == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function validConnexion($email,$password)
    {
        $requete = "SELECT COUNT(*) as Nombre FROM pharma WHERE emailPharma='$email' AND mdpPharma='$password'";
        $resultat = PdoLafleur::$monPdo->query($requete);
        $lesLignes = $resultat->fetch();
        $nombre = $lesLignes['Nombre'];

        if ($nombre == 0)
        {
            return false;
        }
        else
        {
            $requete2 = "SELECT idPharma FROM pharma WHERE emailPharma='$email' AND mdpPharma='$password'";
            $resultat2 = PdoLafleur::$monPdo->query($requete2);
            $lesLignes2 = $resultat2->fetch();
            $_SESSION['idPharma'] = $lesLignes2['idPharma'];
            return true;
        }
    }

    public function listeAnimation($idPharmacie)
    {
        $requete = "SELECT * FROM demande WHERE idPharma='$idPharmacie'";
        $resultat = PdoLafleur::$monPdo->query($requete);
        $lesDemandes = $resultat->fetchAll();
        return $lesDemandes;
    }

    public function lesProduits()
    {
        $requete = "SELECT * FROM produit";
        $resultat = PdoLafleur::$monPdo->query($requete);
        $lesProduits = $resultat->fetchAll();
        return $lesProduits;
    }

    public function validerAnimation($idProduit,$date,$debut,$fin,$remarque)
    {
        $idPharmacie = $_SESSION['idPharma'];
        $requete = "INSERT INTO demande(`idDemande`, `dateDemande`, `debut`, `fin`, `remarque`, `status`, `remGSB`, `idDemande_1`, `idProduit`, `idUser`, `idPharma`) VALUES ('',$date,$debut,$fin,$remarque,'','','',$idProduit,'',$idPharmacie)";
        return $requete;//$resultat = PdoLafleur::$monPdo->query($requete);
    }
}

?>