<?php

if(isset($_REQUEST['action']))
{
    $action = $_REQUEST['action'];
}
else
{
    $action = 'connecter';
}

switch($action)
{
    case 'connecter':
    {
        include("vues/v_connexion.php");
        break;
    }
    case 'validInscription':
    {
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $adresse = $_POST['adresse'];
        $cp = $_POST['cp'];
        $ville = $_POST['ville'];
        $mail = $_POST['mail'];
        $pwd = $_POST['pwd'];

        $issue = $pdo->inscriptionClient($nom,$prenom,$adresse,$cp,$ville,$mail,$pwd);
        $msgErreurs[]  = "Inscription Effectuer";
        include("vues/v_erreurs.php");
        break;
    }
    case 'validConnexion':
    {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $informations = $pdo->validConnexion($email, $password);
        if($informations == true)
        {
            $_SESSION['isLog'] = true;
            include("vues/v_accueilPharma.php");
        }
        else
        {
            $msgErreurs[]  = "Erreur de login";
            include("vues/v_erreurs.php");
        }
        break;
    }
    case 'demandeAnimation':
    {
        $lesProduits = $pdo->lesProduits();
        include("vues/v_demandeAnimation.php");
        break;
    }
    case 'validerAnimation':
    {
        $idProduit = $_POST['listeProduit'];
        $date = $_POST['date'];
        $debut = $_POST['debut'];
        $fin = $_POST['fin'];
        $remarque = $_POST['remarque'];

        $informations = $pdo->validerAnimation($idProduit,$date,$debut,$fin,$remarque);
        echo $informations;
        include("vues/v_demandeAnimation.php");
        break;
    }
    case 'listeAnimation':
    {
        $idPharmacie = $_SESSION['idPharma'];
        $lesDemandes = $pdo->listeAnimation($idPharmacie);
        include("vues/v_lesDemandes.php");
        break;
    }
}
?>
